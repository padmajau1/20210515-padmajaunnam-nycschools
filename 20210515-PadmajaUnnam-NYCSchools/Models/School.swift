//
//  School.swift
//  20210515-PadmajaUnnam-NYCSchools
//
//  Created by PadmajaU on 5/15/21.
//

struct School: Decodable {
    let id : String
    let name: String
    
    let phoneNumber: String
    let email: String?
    let website: String
    
    let addresLine: String
    let city: String
    let zip: String
    let state: String
    let latitude: String
    let longitude: String
    
    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case name = "school_name"
        case phoneNumber = "phone_number"
        case email = "school_email"
        case website = "website"
        case latitude = "latitude"
        case longitude = "longitude"
        case addresLine = "primary_address_line_1"
        case city
        case zip
        case state = "state_code"
    }
}

struct SATScore: Decodable {
    let id: String
    let schoolName: String
    let numOfTestTakers: String
    let readingScore: String
    let writingScore: String
    let mathScore: String
    
    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case schoolName = "school_name"
        case numOfTestTakers = "num_of_sat_test_takers"
        case readingScore = "sat_critical_reading_avg_score"
        case writingScore = "sat_writing_avg_score"
        case mathScore = "sat_math_avg_score"
    }
}

extension School {
    static var fake: School {
        let school = School(id: "01M448", name: "University Neighborhood High School", phoneNumber: "212-962-4341", email: "KChu@schools.nyc.gov", website: "www.universityneighborhoodhs.org", addresLine: "West St", city: "Rochester", zip: "74657", state: "NY", latitude: "40.71227", longitude: "-73.9841")
        return school
    }
}

extension SATScore {
    static var fake: SATScore {
        let satScore = SATScore(id: "01M448", schoolName: "University Neighborhood High School", numOfTestTakers: "150", readingScore: "234", writingScore: "345", mathScore: "342")
        return satScore
    }
}

