//
//  NYCSchoolsAPI.swift
//  20210515-PadmajaUnnam-NYCSchools
//
//  Created by PadmajaU on 5/15/21.
//

import Foundation

private let schoolListUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
private let schoolDetailsUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"

/// NYCSchoolsAPI Network Class
/// To Fetch Schools and details from API EndPoints
final class NYCSchoolsAPI {
    static let shared = NYCSchoolsAPI()
    private init() {}
    
    /// Fetch Schools
    /// - Parameters:
    ///   - searchText: search text to the api
    ///   - offset: offset for pagination
    ///   - limit: search limit for search
    ///   - completion: completion handler gets array of schools result
    func fetchSchools(searchText: String, offset: Int, limit: Int, completion: @escaping ([School]) -> Void) {

        let filterFields = URLQueryItem(name: "$select", value: "dbn,school_name,phone_number,school_email,website,latitude,longitude,primary_address_line_1,city,zip,state_code".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed))
        let offset = URLQueryItem(name: "$offset", value: String(offset))
        let limit = URLQueryItem(name: "$limit", value: String(limit))
        let q = URLQueryItem(name: "$q", value: searchText)

        var components = URLComponents(string: schoolListUrl)
        components?.queryItems = [filterFields, offset, limit, q]

        guard let url = components?.url else {
            completion([])
            return
        }

        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            DispatchQueue.main.async {
                guard let _data = data else {
                    completion([])
                    return
                }
                do {
                    let results = try JSONDecoder().decode([School].self, from: _data)
                    completion(results)
                } catch {
                    print("Error", error)
                    completion([])
                }
            }
        }.resume()
    }
    
    /// Fetch Schools Details
    /// - Parameters:
    ///   - id: dbn value
    ///   - completion: completion handler gets SATScore
    func fetchSchoolsDetails(id: String, completion: @escaping (SATScore?) -> Void) {

        let dbn = URLQueryItem(name: "dbn", value: id)

        var components = URLComponents(string: schoolDetailsUrl)
        components?.queryItems = [dbn]

        guard let url = components?.url else {
            completion(nil)
            return
        }

        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            DispatchQueue.main.async {
                guard let _data = data else {
                    completion(nil)
                    return
                }
                do {
                    let satScore = try JSONDecoder().decode([SATScore].self, from: _data).first
                    completion(satScore)
                } catch {
                    completion(nil)
                }
            }
        }.resume()
    }

}
