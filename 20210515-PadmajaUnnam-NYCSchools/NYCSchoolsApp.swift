//
//  NYCSchoolsApp.swift
//  20210515-PadmajaUnnam-NYCSchools
//
//  Created by PadmajaU on 5/15/21.
//

import SwiftUI

@main
struct NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView()
                .environmentObject(SchoolListViewModel())
        }
    }
}
