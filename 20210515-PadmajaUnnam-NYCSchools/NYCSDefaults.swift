//
//  NYCSDefaults.swift
//  20210515-PadmajaUnnam-NYCSchools
//
//  Created by PadmajaU on 5/15/21.
//

import Foundation

private let favoritesKey = "favorites"

/// NYCSchools Defaults
/// Utility class for user defaults to store the id favorited/not
final class NYCSDefaults {

    private var favorites = [String]()

    static let shared = NYCSDefaults()
    
    private init() {
        if let _favorites = UserDefaults.standard.array(forKey: favoritesKey) as? [String] {
            favorites = _favorites
        } else {
            UserDefaults.standard.set(favorites, forKey: favoritesKey)
        }
    }
    
    /// Toggle Favorite
    /// - Parameter id: school id
    func toggleFavorite(id: String) {
        if favorites.contains(id) {
            favorites.removeAll(where: { $0 == id})
        } else {
            favorites.append(id)
        }
        UserDefaults.standard.set(favorites, forKey: favoritesKey)
        UserDefaults.standard.synchronize()
    }
    
    /// IsFavorite Function
    /// - Parameter id: school id
    /// - Returns: Bool if favorite or not
    func isFavorite(id: String) -> Bool {
        favorites.contains(id)
    }
}
