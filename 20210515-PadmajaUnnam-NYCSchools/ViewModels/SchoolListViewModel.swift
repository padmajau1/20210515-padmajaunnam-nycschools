//
//  SchoolListViewModel.swift
//  20210515-PadmajaUnnam-NYCSchools
//
//  Created by PadmajaU on 5/16/21.
//

import Combine
import Foundation

/// School List ViewModel
class SchoolListViewModel: ObservableObject {
    @Published var schools: [SchoolViewModel] = []
    private var currentPage = 1
    private let limit = 20

    @Published var isLoadingPage = false
    private var canLoadMorePages = true

    private var currentSearchText: String = ""

    init() {
        loadMoreContent()
    }

    
    /// loadMoreContentIfNeeded
    /// - Parameter item: view model
    func loadMoreContentIfNeeded(currentItem item: SchoolViewModel?) {
        guard let item = item else {
            loadMoreContent()
            return
        }

        let thresholdIndex = schools.index(schools.endIndex, offsetBy: -5)
        if schools.lastIndex(where: { $0.id == item.id }) == thresholdIndex {
            loadMoreContent()
        }
    }
    
    /// loadMoreContent
    private func loadMoreContent() {
        guard !isLoadingPage && canLoadMorePages else {
            return
        }

        isLoadingPage = true
        NYCSchoolsAPI.shared.fetchSchools(searchText: currentSearchText, offset: currentPage, limit: limit) { [weak self] schools in
            self?.schools.append(contentsOf: schools.map({ SchoolViewModel(school: $0) }))
            self?.isLoadingPage = false
            self?.currentPage += 1
        }
    }
    
    /// searchSchools
    /// - Parameter searchText: String for search
    func searchSchools(searchText: String) {
        self.currentPage = 1
        self.currentSearchText = searchText
        isLoadingPage = true
        NYCSchoolsAPI.shared.fetchSchools(searchText: searchText, offset: 0, limit: limit) { [weak self] schools in
            self?.schools = schools.map({ SchoolViewModel(school: $0) })
            self?.isLoadingPage = false
        }
    }
}
