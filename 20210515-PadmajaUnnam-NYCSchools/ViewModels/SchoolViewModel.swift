//
//  SchoolViewModel.swift
//  20210515-PadmajaUnnam-NYCSchools
//
//  Created by PadmajaU on 5/16/21.
//

import Combine
import CoreLocation
import Foundation

///School View Model
final class SchoolViewModel: Identifiable, ObservableObject {

    var id = UUID()

    private let school: School
    @Published private(set) var isFavorite: Bool = false
    
    @Published
    var satScore: SATScore?
    
    init(school: School) {
        self.school = school
        isFavorite = NYCSDefaults.shared.isFavorite(id: school.id)
    }
    
    /// School name
    var name: String {
        return school.name
    }
    
    /// Phone Number of School
    var phoneNumber: String {
        return school.phoneNumber
    }
    
    /// Email of the School
    var email: String {
        return school.email ?? "N/A"
    }
    
    /// website url of the School
    var websiteURL: URL? {
        return URL(string: "https://"+school.website)
    }
    
    var website: String {
        return school.website
    }
    /// address of the School
    var address: String {
        return school.addresLine + ", " + school.city + ", " + school.state + ", " + school.zip
    }

    var coordinate: CLLocationCoordinate2D? {
        guard let lat = Double(school.latitude), let long = Double(school.longitude) else {
            return nil
        }
        return CLLocationCoordinate2D(latitude: lat, longitude: long)
    }
    
    
    /// reading score
    var readingScore: String {
        return satScore?.readingScore ?? "N/A"
    }
    
    /// writing score
    var writingScore: String {
        return satScore?.writingScore ?? "N/A"
    }
    
    /// Math score
    var mathScore: String {
        return satScore?.mathScore ?? "N/A"
    }
    
    
    /// Favorite Toggle
    func toggleFavorite() {
        NYCSDefaults.shared.toggleFavorite(id: school.id)
        isFavorite = NYCSDefaults.shared.isFavorite(id: school.id)
    }

    /// Fetching School Details
    func fetchSchoolDetails() {
        guard satScore == nil else { return }
        NYCSchoolsAPI.shared.fetchSchoolsDetails(id: school.id) { [weak self] score in
            self?.satScore = score
        }
    }
}
