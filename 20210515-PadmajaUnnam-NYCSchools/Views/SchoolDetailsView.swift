//
//  SchoolDetailsView.swift
//  20210515-PadmajaUnnam-NYCSchools
//
//  Created by PadmajaU on 5/15/21.
//

import MapKit
import SwiftUI

/// School Details View
/// Each School Details View
struct SchoolDetailsView: View {
    
    @EnvironmentObject var viewModel: SchoolViewModel
    
    @State private var region = MKCoordinateRegion()
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                HStack {
                    Text(viewModel.name)
                        .font(.title)
                        .bold()
                    Spacer()
                    Button(action: {
                        viewModel.toggleFavorite()
                    }, label: {
                        Image(systemName: viewModel.isFavorite ? "heart.fill" : "heart")
                            .font(.title)
                            .foregroundColor(.red)
                    })
                }
                .padding(.bottom, 10)
                Text("SAT Scores")
                    .font(.title3)
                    .bold()
                VStack(alignment: .leading, spacing: 5) {
                    
                    HStack(spacing: 0) {
                        VStack {
                            Text("Reading")
                            Divider().foregroundColor(Color.black)
                            Text(viewModel.readingScore)
                                .font(.body)
                        }
                        .frame(maxWidth: .infinity)
                        Divider()
                        
                        VStack {
                            Text("Writing")
                            Divider()
                            Text(viewModel.writingScore)
                                .font(.body)
                        }
                        .frame(maxWidth: .infinity)
                        Divider()
                        
                        
                        VStack {
                            Text("Math")
                            Divider()
                            Text(viewModel.mathScore)
                                .font(.body)
                        }
                        .frame(maxWidth: .infinity)
                        Divider()
                    }
                    .frame(height: 80)
                    .border(Color.gray)
                    .padding(.bottom, 10)
                }
                VStack(alignment: .leading, spacing: 5) {
                    Text("Contact")
                        .font(.title3)
                        .bold()
                    Text(viewModel.address)
                        .font(.subheadline)
                        .padding(.bottom, 5)
                    HStack {
                        Image(systemName: "phone")
                        Button(action: {
                            let telephone = "tel://"
                            let formattedString = telephone + viewModel.phoneNumber
                            guard let url = URL(string: formattedString) else { return }
                            UIApplication.shared.open(url)
                        }) {
                            Text(viewModel.phoneNumber)
                                .font(.subheadline)
                        }
                    }
                    HStack {
                        Image(systemName: "envelope")
                        Button(action: {
                            guard let url = URL(string: "mailto:\(viewModel.email)") else { return }
                            UIApplication.shared.open(url)
                        }) {
                            Text(viewModel.email)
                                .font(.subheadline)
                        }
                    }
                    
                    if let url = viewModel.websiteURL {
                        HStack {
                            Image(systemName: "globe")
                            Link(viewModel.website, destination: url)
                                .font(.subheadline)
                        }
                    }
                }
                .padding(.bottom, 10)
                if let coordinate = viewModel.coordinate {
                    Map(coordinateRegion: $region,showsUserLocation: true, annotationItems: [self.viewModel]){ viewModel in
                        MapAnnotation(coordinate: coordinate) {
                            Circle()
                                .fill(Color.red)
                                .frame(width: 20, height: 20)
                                .onTapGesture(count: 1, perform: {
                                    let place = MKPlacemark(coordinate: coordinate)
                                    let mapItem = MKMapItem(placemark: place)
                                    mapItem.name = viewModel.name
                                    mapItem.openInMaps(launchOptions: nil)
                                })
                        }
                    }
                    .aspectRatio(1.5, contentMode: .fill)
                    .onAppear{
                        setRegion(coordinate)
                    }
                }
                Spacer()
            }
            
            .onAppear(perform: {
                viewModel.fetchSchoolDetails()
            })
            .padding()
            .navigationBarHidden(false)
            .navigationTitle("SAT Score")
            .navigationBarTitleDisplayMode(.inline)
        }
        Spacer()
    }
    
    /// setRegion
    /// - Parameter coordinate: coordinate to set region
    private func setRegion(_ coordinate: CLLocationCoordinate2D){
            region = MKCoordinateRegion(
                center: coordinate,
                span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2)
            )
        }
}

struct SchoolDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        let schoolVM = SchoolViewModel(school: School.fake)
        schoolVM.satScore = SATScore.fake
        return SchoolDetailsView()
                .environmentObject(schoolVM)
    }
}
