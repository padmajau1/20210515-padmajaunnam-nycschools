//
//  HomeView.swift
//  20210515-PadmajaUnnam-NYCSchools
//
//  Created by PadmajaU on 5/15/21.
//

import SwiftUI

/// Home View
/// List of schools
struct HomeView: View {
    @State private var searchText = ""
    @State private var showCancelButton: Bool = false

    @EnvironmentObject var viewModel : SchoolListViewModel

    var body: some View {
        NavigationView {
            VStack {
                VStack(alignment: .leading)  {
                    SearchBar(text: $searchText, placeholder: "Search Schools", onSearchAction: searchSchools(for:))
                    if !viewModel.isLoadingPage, viewModel.schools.isEmpty {
                        Text("No Schools Found")
                    } else {
                        ScrollView {
                            LazyVStack(alignment: .leading)  {
                                ForEach(viewModel.schools) { schoolVM in
                                    SchoolTileView()
                                        .environmentObject(schoolVM)
                                        .onAppear(perform: {
                                            viewModel.loadMoreContentIfNeeded(currentItem: schoolVM)
                                        })
                                }
                            }
                            .padding()
                        }
                    }
                }
                if viewModel.isLoadingPage {
                    VStack(alignment: .center, content: {
                        ProgressView()
                            .frame(alignment: .center)
                    })
                }
                Spacer()
            }
            .navigationTitle("NYC Schools")
            .onAppear(perform: {
                self.searchSchools(for: searchText)
            })
        }
    }
    
    /// Search Schools
    /// - Parameter searchText: Search schools with search text
    func searchSchools(for searchText: String) {
        viewModel.searchSchools(searchText: searchText)
     }
}

extension UIApplication {
    func endEditing(_ force: Bool) {
        self.windows
            .filter{$0.isKeyWindow}
            .first?
            .endEditing(force)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let vm = SchoolListViewModel()
        vm.schools = [SchoolViewModel(school: School.fake)]
        return HomeView()
                .environmentObject(vm)
    }
}
