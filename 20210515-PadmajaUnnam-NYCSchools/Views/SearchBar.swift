//
//  SearchBar.swift
//  20210515-PadmajaUnnam-NYCSchools
//
//  Created by PadmajaU on 5/15/21.
//

import SwiftUI

/// SearchBar
struct SearchBar: UIViewRepresentable {

    @Binding var text: String
    var placeholder: String
    var onSearchAction: (String) -> Void
    
    class Coordinator: NSObject, UISearchBarDelegate {

        @Binding var text: String
        var onSearchAction: (String) -> Void

        init(text: Binding<String>, onSearchAction: @escaping (String) -> Void ) {
            _text = text
            self.onSearchAction = onSearchAction
        }
        
        /// UISearchBar Delagate Methods
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            text = searchText
        }

        func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
            searchBar.showsCancelButton = true
        }

        func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
            searchBar.showsCancelButton = false
        }

        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            searchBar.resignFirstResponder()
            self.text = ""
            onSearchAction("")
        }

        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            searchBar.resignFirstResponder()
            onSearchAction(searchBar.text ?? "")
        }
    }
    
    /// Make Coordinator
    /// - Returns: Search Bar Coordinator
    func makeCoordinator() -> SearchBar.Coordinator {
        return Coordinator(text: $text, onSearchAction: onSearchAction)
    }
    
    /// Making UISearchBar
    /// - Parameter context: SeachBar Representable Context
    /// - Returns: SearchBar
    func makeUIView(context: UIViewRepresentableContext<SearchBar>) -> UISearchBar {
        let searchBar = UISearchBar(frame: .zero)
        searchBar.delegate = context.coordinator
        searchBar.placeholder = placeholder
        searchBar.searchBarStyle = .minimal
        return searchBar
    }

    func updateUIView(_ uiView: UISearchBar, context: UIViewRepresentableContext<SearchBar>) {
        uiView.text = text
    }
}
