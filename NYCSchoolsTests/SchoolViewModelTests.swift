//
//  SchoolViewModelTests.swift
//  NYCSchoolsTests
//
//  Created by PadmajaU on 5/16/21.
//

import XCTest
import CoreLocation
@testable import NYCSchools

class SchoolViewModelTests: XCTestCase {
    
    var viewModel: SchoolViewModel!
    let school = School.fake
    let satScore = SATScore.fake

    override func setUp() {
        super.setUp()
        viewModel = SchoolViewModel(school: school)
        viewModel.satScore = satScore
    }

    func testProperties() {
        XCTAssertEqual(viewModel.name, school.name, "Invalid name")
        XCTAssertEqual(viewModel.phoneNumber, school.phoneNumber, "Invalid phoneNumber")
        XCTAssertEqual(viewModel.email, school.email, "Invalid email")
        XCTAssertEqual(viewModel.website, school.website, "Invalid website")
    }

    func testAddressFormat() {
        let address = school.addresLine + ", " + school.city + ", " + school.state + ", " + school.zip
        XCTAssertEqual(viewModel.address, address, "Address format is invalid")
    }

    func testCoordinate() {
        if let lat = Double(school.latitude), let long = Double(school.longitude) {
            XCTAssertEqual(viewModel.coordinate?.latitude, lat, "Invalid latitude")
            XCTAssertEqual(viewModel.coordinate?.longitude, long, "Invalid longitude")
        }
    }

    func testSATScores() {
        XCTAssertEqual(viewModel.readingScore, satScore.readingScore, "Invalid readingScore")
        XCTAssertEqual(viewModel.writingScore, satScore.writingScore, "Invalid writingScore")
        XCTAssertEqual(viewModel.mathScore, satScore.mathScore, "Invalid mathScore")
    }

    func testAddFavorite() {
        //Add Favorite
        viewModel.toggleFavorite()
        XCTAssertTrue(viewModel.isFavorite, "School should be favorite")

        XCTAssertTrue(NYCSDefaults.shared.isFavorite(id: school.id), "School should be favorite")

        //Remove Favorite
        viewModel.toggleFavorite()
        XCTAssertFalse(viewModel.isFavorite, "School should not be favorite")

        XCTAssertFalse(NYCSDefaults.shared.isFavorite(id: school.id), "School should not be favorite")
    }
}

